#!/usr/bin/python -u

import asyncore
import socket
import os
from time import time

__all__ = ['TCPProxy']


class TCPProxy(asyncore.dispatcher):

    def __init__(
            self,
            external_addr, external_port,
            internal_addr, internal_port,
            logs_dir):
        self.internal_port = internal_port
        self.internal_addr = internal_addr
        self.external_port = external_port
        self.external_addr = external_addr
        self.logs_dir = logs_dir
        # Create listen socket
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.set_reuse_addr()
        self.bind((external_addr, external_port))
        self.listen(200)

    def handle_accept(self):
        # Accept connection from client
        client_socket, client_address = self.accept()
        # Create logger
        file_name = '%s/%.8f_%d' % (
            self.logs_dir, time(), client_address[1])
        f = open(file_name, 'ab')
        logger = Logger(f)
        # Create connection with client (binded with file logger)
        client_connection = ExternalConnection(
            client_socket, client_address, logger)
        # Create connection with internal server (binded with client
        # connection)
        InternalConnection(
            client_connection, self.internal_addr, self.internal_port)

    def handle_close(self):
        self.close()


class Logger(asyncore.file_dispatcher):

    def __init__(self, f):
        asyncore.file_dispatcher.__init__(self, f)
        # Random delimiter
        self.delim = os.urandom(16).encode('hex')
        # Data to be written TO FILE
        self.buffer = self.delim
        # State of binded client connection
        self.client_connection_closed = False

    def logInput(self, data):
        self.buffer += '{0}[{1}]{0}'.format(self.delim, data)

    def logOutput(self, data):
        self.buffer += '{0}{{{1}}}{0}'.format(self.delim, data)

    def readable(self):
        return False

    def writable(self):
        if self.buffer == '' and self.client_connection_closed:
            self.close()
        return (len(self.buffer) > 0)

    def handle_write(self):
        sent = self.send(self.buffer)
        self.buffer = self.buffer[sent:]

    def handle_close(self):
        self.close()


class ExternalConnection(asyncore.dispatcher):

    def __init__(self, client_socket, client_address, logger):
        asyncore.dispatcher.__init__(self, client_socket)
        # Data FROM CLIENT
        self.input_buffer = ''
        # Data to be sent TO CLIENT
        self.output_buffer = ''
        # Binded internal connection
        self.internal_connection = None
        # Binded logger
        self.logger = logger
        # State of binded internal connection
        self.internal_connection_closed = False

    def handle_read(self):
        d = self.recv(4096)
        self.logger.logInput(d)
        self.input_buffer += d

    def writable(self):
        if self.output_buffer == '' and self.internal_connection_closed:
            self.close()
            self.logger.client_connection_closed = True
        return (len(self.output_buffer) > 0)

    def handle_write(self):
        sent = self.send(self.output_buffer)
        self.logger.logOutput(self.output_buffer[:sent])
        self.output_buffer = self.output_buffer[sent:]

    def handle_close(self):
        self.close()
        self.logger.client_connection_closed = True
        if self.internal_connection:
            self.internal_connection.close()


class InternalConnection(asyncore.dispatcher):

    def __init__(self, client_connection, internal_addr, internal_port):
        # Connect to internal server
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.connect((internal_addr, internal_port))
        # Bind client connection and internal connection
        self.client_connection = client_connection
        client_connection.internal_connection = self

    def handle_read(self):
        d = self.recv(4096)
        self.client_connection.output_buffer += d

    def writable(self):
        return (len(self.client_connection.input_buffer) > 0)

    def handle_write(self):
        sent = self.send(self.client_connection.input_buffer)
        self.client_connection.input_buffer = \
            self.client_connection.input_buffer[sent:]

    def handle_close(self):
        self.close()
        self.client_connection.internal_connection_closed = True
